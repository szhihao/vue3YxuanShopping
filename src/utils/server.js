import axios from 'axios'
import {
    Toast
} from 'vant';
const server = axios.create({
    baseURL: 'https://api.it120.cc',
    timeout: 5000,
})
server.interceptors.request.use(config => { //使用请求拦截    
    // config包含了请求相关的所有信息
    config.headers.token = localStorage.getItem('token')
    Toast.loading({
        message: '加载中...',
        forbidClick: true,
        loadingType: 'spinner',
        duration: 0,
    });
    return config // 将配置完成的token 返回 如果不返回请求就不会执行
}, err => {
    // 请求发生错误的回调函数
    // throw new Error(err) //抛出
    // console.warn(err);
    // console.error(err);
    Promise.reject(err)
})

server.interceptors.response.use(res => {
    // 服务器返回的东西
    if (res.status == 200) { // 响应拦截这边要要清楚一下
        Toast.clear()
    }
    return res.data // 这边要返回一个data

}, err => {
    Promise.reject(err)
})

export default server
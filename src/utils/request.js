import server from './server'
// 这里就是axios 

export default function request({url = '', methods = 'post', data = {}, params = {}}) {
    return server({
        url,
        methods,
        data,
        params
    })
}
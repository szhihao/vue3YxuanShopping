import request from "../utils/request";

// 城市的数据
const city = async () => {
    let {
        data
    } = await request({
        url: '/common/region/province?pid'
    })
    return data
}
// 县区数据
const county = async (id) => {
    let {
        data
    } = await request({
        url: '/common/region/child?pid=' + id
    })
    return data
}


// 分类的接口数据
const classify = async () => {
    let {
        data
    } = await request({
        url: '/small4/shop/goods/category/all '
    })
    return data
}
// 猜你喜欢的接口
const love = async () => {
    let {
        data
    } = await request({
        url: '/small4/shop/goods/list',
    })
    return data
}
// 专栏详情
const details = async (id) => {
    let {
        data
    } = await request({
        url: '/small4/cms/news/detail?id=' + id,
    })
    return data
}

// 签到的接口
const punch = async (token) => {
    let {
        data
    } = await request({
        url: '/small4/score/logs',
        params: {
            token
        }
    })
    return data
}
// 签到的积分

const integral = async () => {
    let {
        data
    } = await request({
        url: '/small4/score/sign/rules',
    })
    return data
}





export {
    city,
    county,
    classify,
    love,
    details,
    punch,
    integral,
}
// 这里封装api请求模块

import request from "../utils/request";
const getBanners = async () => {
    let {
        data
    } = await request({
        url: '/small4/banner/list'
    })
    return data
}

const getCardList = async () => {
    let {
        data
    } = await request({
        url: '/small4/shop/goods/kanjia/list',
    })
    return data
}

const siftObj = async () => {
    let {
        data
    } = await request({
        url: '/small4/cms/news/list'
    })
    return data
}


const moodsList = async () => {
    return await request({
        url: '/small4/shop/goods/list'
    })
}

const getDetails = async (id) => {
    let {
        data
    } = await request({
        url: '/small4/shop/goods/detail?id=' + id
    })
    return data
}


//  请求商品评价
const appraise = async (id) => {
    let {
        data
    } = await request({
        url: '/small4/shop/goods/reputation?goodsId=' + id
    })
    return data
}

// 登录接口验证
const useLogin = async (mobile, pwd) => {
    return await request({
        url: '/small4/user/m/login?deviceId=007&deviceName=monkey',
        params: {
            mobile,
            pwd,
        }
    })
}

// 验证码接口
const phoneCode = async (id) => {
    let {
        data
    } = await request({
        url: '/small4/verification/pic/get?key=' + id,
    })
    return data
}

// 优惠卷的接口
const roll = async (token, id) => {
    console.log('ss' + token);
    console.log('ss' + id);
    let {
        data
    } = await request({
        url: '/small4/discounts/fetch',
        params: {
            id,
            token,

            // Query    参数名	类型	必需	描述	示例 e.g.
            // id	    number	是	优惠券ID	
            // pwd	    string	否	口令红包必须传	
            // token	string	是	调用登录接口获取的登录凭证	
            // detect 
        }
    })
    return data
}
// 地址的接口
const city = async (token) => {
    console.log(token);
    let {
        data
    } = await request({
        url: '/small4/user/shipping-address/list',
        params: {
            token
        }
    })
    return data
}
// 添加地址信息的接口
const addCity = async (token, obj) => {
    console.log(obj.address[0].code);
    let {
        data
    } = await request({
        url: '/small4/user/shipping-address/add',
        params: {
            token: token,
            address: obj.school,
            cityId: obj.address[1].code,
            code: obj.mail,
            linkMan: obj.name,
            mobile: obj.phone,
            provinceId: obj.address[0].code,
        },
    })
    return data
}


// 请勿重新签到的接口
const notSignin = async (token) => {
    console.log('有token了' + token);
    let {
        data
    } = await request({
        url: '/small4/score/sign',
        params: {
            token
        }
    })
    return data
}
// 确认付款 
const payment = async (token) => {
    let {
        data
    } = await request({
        url: '/small4/user/shipping-address/default',
        params: {
            token
        }
    })
    return data
}

// 订单评价
const orderEvaluate = async (token) => {
    let {
        data
    } = await request({
        url: '/small4/order/list',
        params: {
            token
        }
    })
    return data
}


// 优惠卷
const rollCards = async () => {
    let {
        data
    } = await request({
        url: '/small4/discounts/coupons',
    })
    return data
}

// 我的购物订单
const myOrder = async (token) => {
    let {
        data
    } = await request({
        url: '/small4/order/list',
        params: {
            token
        }
    })
    return data
}

// 我的优惠卷
const HomeRolls = async (token) => {
    let {
        data
    } = await request({
        url: '/small4/discounts/my',
        params: {
            token
        }
    })
    return data
}


export {
    getBanners,
    getCardList,
    siftObj,
    moodsList,
    getDetails,
    appraise,
    useLogin,
    phoneCode,
    roll,
    city,
    addCity,
    notSignin,
    payment,
    orderEvaluate,
    rollCards,
    myOrder,
    HomeRolls
}
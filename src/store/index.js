import {
  createStore
} from 'vuex'
import persis from 'vuex-persistedstate'
import { Toast } from "vant";
export default createStore({
  state: {
    shoppingList: [], // 购物车数据
    checkAll: false, // 表示底部的全选
  },
  mutations: {
    pushShopping(state, value) {
      let flag = true
      state.shoppingList.forEach(item => {

        if (item.upid == value.upid && item.botid == value.botid) { // 如果相同就加num
          item.num += value.num
          flag = false
          console.log('重复了');
        }
      })
      if (flag) {
        state.shoppingList.push(value)
      }
    },

    checkAllChange(state, value) {
      console.log(value);
      state.shoppingList.forEach(item => {
        item.checked = value
      })
    },
    removeAll(state) {
      state.shoppingList = state.shoppingList.filter(item => !item.checked)
    },
    removerCommodity(state, value) {
      //  删除单个商品
      state.shoppingList =  state.shoppingList.filter(item  => item.upid != value.upid)
      Toast.success(`删除${value.name}成功`)
    }
  },
  actions: {},
  modules: {},
  getters: {
    total(value) {
      return value.shoppingList.reduce(
        (num, item) => (num += item.checked && item.num * item.minPrice),
        0
      )
    }
  },
  plugins: [persis()]
})
import {
  createRouter,
  createWebHashHistory
} from 'vue-router'

import homeModel from '../views/homeModel/route'
const routes = [{
    path: '/',
    name: 'index',
    component: () => import('../views/index.vue'),
    children: homeModel
  },
  {
    path: '/bargainBox',
    name: 'bargainBox',
    component: () => import('../views/bargainPage/bargainBox.vue'),
    meta: {
      title: '砍价页面',
      intercept: false,
    }
  },
  {
    path: '/strictCard',
    name: 'strictCard',
    component: () => import('../components/cardComponents/strictCard.vue'),
    meta: {
      title: '严选专栏',
      intercept: false,
    }
  },
  {
    path: '/moods',
    name: 'moods',
    component: () => import('../components/moods.vue'),
    meta: {
      title: '人气推荐',
      intercept: false,
    }
  },

  {
    path: '/details',
    name: 'details',
    component: () => import('../components/slot/details.vue'),
    meta: {
      title: '详情页面',
      intercept: false,
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../components/login/login.vue'),
    meta: {
      title: '登录页面',
      intercept: false,
    }
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../components/login/register.vue'),
    meta: {
      title: '注册页面',
      intercept: false,
    }
  },
  {
    path: '/lookDetails',
    name: 'lookDetails',
    component: () => import('../components/cardComponents/lookDetails.vue'),
    meta: {
      title: '详情列表',
      intercept: false,
    }
  },
  {
    path: '/signin',
    name: 'signin',
    component: () => import('../components/signin/signin.vue'),
    meta: {
      title: '签到页面',
      intercept: true,
    }
  },
  {
    path: '/bargain',
    name: 'bargain',
    component: () => import('../components/bargain/bargain.vue'),
    meta: {
      title: '砍价页面',
      intercept: false,
    }
  },
  {
    path: '/barginList',
    name: 'barginList',
    component: () => import('../components/bargain/barginList.vue'),
    meta: {
      title: '砍价详情',
      intercept: false,
    }
  },
  {
    path: '/roll',
    name: 'roll',
    component: () => import('../components/roll/roll.vue'),
    meta: {
      title: '礼券页面',
      intercept: false,
    }
  },
  {
    path: '/site',
    name: 'site',
    component: () => import('../components/site/site.vue'),
    meta: {
      title: '地址页面',
      intercept: false,
    }
  },
  {
    path: '/newsite',
    name: 'newsite',
    component: () => import('../components/site/newsite.vue'),
    meta: {
      title: '新建地址',
      intercept: false,
    }
  },
  {
    path: '/order',
    name: 'order',
    component: () => import('../components/order/order.vue'),
    meta: {
      title: '确认订单',
      intercept: false,
    }
  },

  {
    path: '/payment',
    name: 'payment',
    component: () => import('../components/order/payment.vue'),
    meta: {
      title: '下单吧',
      intercept: false,
    }
  },
  {
    path: '/myEvaluate',
    name: 'myEvaluate',
    component: () => import('../components/order/myEvaluate.vue'),
    meta: {
      title: '评价页面',
      intercept: true,
    }

  },
  {
    path: '/myOrder',
    name: 'myOrder',
    component: () => import('../components/order/myOrder.vue'),
    meta: {
      title: '我的订单',
      intercept: true,
    }
  },
  {
    path: '/myRoll',
    name: 'myRoll',
    component: () => import('../components/roll/myRoll.vue'),
    meta: {
      title: '我的礼券',
      intercept: true,
    }
  }



]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, go) => {
  let token = localStorage.getItem('token')
  if (token && token != 'null') { // 证明有用户  
    return true //本地存储有token 用户可以进入页面
  } else {
    if (to.meta.intercept) { // to.meta.intercept  为true 说名要拦截这个页面
      return {
        path: '/login',
        // query: {
        //   ...to.query
        // }
      } // return相当于vue2 的next
    } else { // 否则就可以进去这个页面
      return true
    }

  }
})



router.afterEach((to, next) => {
  document.querySelector('title').innerText = to.meta.title
})

export default router
export default [

    {
        path: '/',
        name: 'homePage',
        component: () => import('./homePage.vue'),
        meta: {
            title: '首页',
            intercept: false,
        }
    }, {
        path: '/classfiy',
        name: 'classfiy',
        component: () => import('./classfiy.vue'),
        meta: {
            title: '分类',
            intercept: false,
        }
    }, {
        path: '/shopping',
        name: 'shopping',
        component: () => import('./shopping.vue'),
        meta: {
            title: '购物',
            intercept: true,
        }
    }, {
        path: '/person',
        name: 'person',
        component: () => import('./person.vue'),
        meta: {
            title: '个人',
            intercept: false,
        }
    },



]
import {
    createApp
} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'amfe-flexible'
import vant from 'vant'
import 'vant/lib/index.css';
import './assets/reset.css'
import './assets/common.css'

createApp(App).use(store).use(router).use(vant).mount('#app')